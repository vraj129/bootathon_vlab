# Bresenham Line Drawing Algorithm

Bresenham's line algorithm. Bresenham's line algorithm is a line drawing algorithm that determines the points of an n-dimensional raster that should be selected in order to form a close approximation to a straight line between two points. 

> ### Aim
To study Bresenham Line Drawing Algorithm

> ### Theory
- Bresenham’s Line drawing algorithm is an accurate, efficient raster line drawing algorithm, that scan converts line using only incremental integer calculations.
- Let us consider the starting point (x1, y1) of a given gentle slope line, we step to each successive column (x direction) and plot the pixel whose scan-line ‘y’ value closest to the actual line path. 

    <img src="th1.png" alt="image not found">

- Now if we consider the next point to be plotted with respect to the point(x1,y1) we may select the point as either (x1+1,y1) or (x1+1,y1+1) as shown in figure 1. • For the selection of the next point Bresenham algorithm uses the decision parameter, to decide whether which point is closer to the actual line segment. 
- Algorithm considers the vertical distances between the specified point (x1+1, y1) and (x1+1, y1+1) with respect to the line segment. 
- As it is observed that for the gentle slope line we will be having more points on X direction, so we have to always increment in X-direction respectively. 
- With the increment in X direction, the decision parameter will decide the increment in y direction as either (0/1) for the gentle slope line. 
- Bresenham algorithm can work efficiently and provides more end point accuracy for the gentle slope lines as well as sharp slope lines. 
  
> ### Procedure
### Algorithm

    1. Start.
    2. Accept end points of a line segment (x1,y1),(x2,y2).
    3. Calculate:
    dx = abs(x2-x1)
    dy = abs(y2-y1)
    4. Calculate the Length of the Line segment as:
    if ( dx >= dy )
    then
             length = dx
    else
             length = dy
    5. Calculate Initial Decision Parameter as:
    d = 2 dy - dx
    6. Calculate:
    incr1 = 2 * dy – 2 * dx
    incr2 = 2 * dy
    7.Calculate xincr as:
    If ( x2 > x1 )
    then
             yincr = 1
    else
             yincr = -1
    8.Initialize:
    x=x1
    y=y1
    i=1
    9.while(i <= length)
    {
             plotpoint (x ,y)
             if(d>=0)
             then
                      x = x + xincr
                      y = y + yincr
                      d = d + incr1
             else
                      x = x + xincr
                      d = d + incr2
             i++
    }
    10. Stop.
